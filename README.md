# Support Bot [![Build status](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot/badges/master/pipeline.svg)](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot/commits/master)

## Installation

```
git clone https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-bot
cd gitlab-support-bot
bundle install
```

## Deployment

### Generate API tokens

**Slack**

+ https://gitlab.slack.com/apps/manage/custom-integrations
+ Click "Bots"
+ Click "Add Configuration"

**Zendesk**

To request an API token, open a [New Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) and select the **API Token Request** template from the dropdown.

### Production deployment

The production deployment of support-bot is done via the `deploy` job in `.gitlab-ci.yml`.

The following variables under - https://gitlab.com/gitlab-com/dev-resources/settings/ci_cd are used:
  + `SLACK_API_TOKEN`
  + `ZD_TOKEN`
  + `GCP_CREDENTIALS`
  + `GCP_PROJECT_ID`
  + `GCP_ZONE`

## Slack Commands

You would use any of the commands by calling `sb <command>` in Slack.  `sb` is
the name of the configured bot in Slack.

```bash
sb sh
```

**Example Output**

```
Needs Org & Triage: 0
Self-Managed with SLA: 61
Total: 61 :fire:
```

To get a list over available commands send a direct message to the bot with `help`.

## Development

Have (or create) a Slack bot for development, and a Zendesk token.

Create a `.env` file in the project directory and fill it with the appropriate
tokens for development:

```bash
ZD_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
SLACK_API_TOKEN="xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
SB_ENVIRONMENT="dev"
SLACK_RUBY_BOT_ALIASES=sbtest
SUPPORT_TEAM_SIZE=101
```

This file is *_not_* committed and is unique to your machine.

Run the server in a new tab with `foreman start`.  Find the bot that you have created in slack using Direct Messages. Then send this development bot a PM
in Slack to try out the commands (ex: `sbtest s`).  Restart the
server when you make changes to the code.

## Troubleshooting

### Things to know

- The bot runs on [Google Cloud](https://console.cloud.google.com/compute/instancesDetail/zones/europe-west4-b/instances/gitlab-supportbot?project=support-testing-168620).
  - We use Docker to deploy the Support Bot image.
  - The Docker container is configured to always restart using the `--restart always` flag at runtime.

- In `home/core` you'll find `supportbot_healthcheck.sh`, a script that inspects the container and if it is reporting unhealthy or stopped restarts it. It also writes a brief status message to `/var/log/supportbot/healthcheck.log`.
  - The health check script is executed hourly using `systemd` timer. See `/etc/systemd/system/healthcheck.service` and `/etc/systemd/system/healthcheck.timer` for how it's configured.

### How do I get SSH access to the droplet?

We connect to the server using Gcloud `gcloud compute ssh --zone europe-west4-b --project support-testing-168620 supportbot@gitlab-supportbot`

### It's Docker


If the bot becomes unpresponsive or goes offline, SSH into the server and check that the `supportbot` container is running and reporting `healthy`.
If it's down, or you think a restart might be useful, do so with `docker restart supportbot`.

You can tail the logs by doing `docker logs -f supportbot` and check if the bot is communicating with Slack/Zendesk.
