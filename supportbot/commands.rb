# frozen_string_literal: true

# Make sure base is loaded before all others
require_relative 'commands/base'

# Load all available commands
Dir[File.join(__dir__, 'commands', '*.rb')].each { |file| require file }
