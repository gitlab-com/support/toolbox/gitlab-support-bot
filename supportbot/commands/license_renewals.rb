# frozen_string_literal: true

module SupportBot
  module Commands
    class LicenseRenewals < Base
      command 'license-renewals', 'lr'
      views :upgrade_renewals_ar
    end
  end
end
