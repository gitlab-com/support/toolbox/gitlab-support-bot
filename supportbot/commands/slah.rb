# frozen_string_literal: true

module SupportBot
  module Commands
    class SLAH < SlackRubyBot::Commands::Base
      include ZendeskHelper

      TICKET_FORM = {
        334_447 => 'SaaS',
        426_148 => 'SM',
        360_000_803_379 => 'Account'
      }.freeze

      command 'slah'

      def self.call(client, data, match)
        case match[:expression]
        when 'status'
          ZendeskHelper.slah_query
          text_response = slah_status
        when 'test'
          text_response = test_message
        else
          text_response = 'Unknown request'
        end

        client.say channel: data.channel, text: text_response
      rescue StandardError => e
        client.say(channel: data.channel, text: "Sorry, #{e.message}.")
      end

      def self.slah_status
        'SLAH is active and running'
      end

      def self.run_slah_loop
        ZendeskHelper.load_cache
        client = SlackRubyBot::Client.new

        loop do
          sleep SupportBot::SLAH_INTERVAL

          tickets = ZendeskHelper.slah_query
          tickets = ZendeskHelper.only_unreported(tickets)

          next if tickets.empty?

          tickets.each do |ticket|
            logger.info "reporting tickets #{ticket.id}, #{SupportBot::SLAH_CHANNEL}"

            text = ticket_text(ticket.id, ticket.subject, ticket.ticket_form_id, ticket.custom_fields)
            client.web_client.chat_postMessage(channel: SupportBot::SLAH_CHANNEL, text: text,
                                               unfurl_links: false, unfurl_media: false)
          end
        rescue StandardError => e
          warn "ERROR: #{e}"
          warn 'Waiting for 5 minutes before restarting bot loop'
          sleep 300
          retry
        end
      end

      def self.test_message
        ticket_text(
          rand(-999_99...-9999),
          'Test message',
          TICKET_FORM.keys.sample,
          [ZendeskAPI::Trackie.new(id: 1234, value: 'ALL')]
        )
      end

      def self.ticket_region_from_custom_field(custom_fields)
        region = custom_fields.find { |field| field.id == 360_018_253_094 }
        return 'ALL' unless region.present?

        case region.value
        when 'americas__usa'
          'AMER'
        when 'asia_pacific'
          'APAC'
        when 'europe__middle_east__africa'
          'EMEA'
        else
          'ALL'
        end
      end

      def self.ticket_text(id, subject, form_id, custom_fields)
        ticket_form = TICKET_FORM[form_id] || 'unknown'

        "[#{ticket_form} - #{ticket_region_from_custom_field(custom_fields)}] <#{SupportBot::ZENDESK_LOCATION}/agent/tickets/#{id}|##{id}> - #{subject}"
      end
    end
  end
end
