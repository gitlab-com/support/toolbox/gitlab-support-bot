# frozen_string_literal: true

require 'terminal-table'

module SupportBot
  module Commands
    class Help < SlackRubyBot::Commands::Base
      command 'help', 'h' do |client, data, _match|
        client.say(channel: data.channel, text: response_table)
      end

      def self.response_table
        table = Terminal::Table.new do |t|
          t.title = "SUPPORTBOT HELP (#{SupportBot::PROJECT_LOCATION})"
          t.headings = ['COMMAND(S)', 'DESCRIPTION']
          t.add_row ['dotcom, dc', 'Only shows the amount of tickets for gitlab.com (subscribers, trials, free)']
          t.add_row ['license-renewals, lr', 'Shows the amount of tickets for License & Renewals']
          t.add_row ['find, search, grep', "Search for a query among new, open, and pending tickets.\nReturns the first #{SupportBot::PER_PAGE} results. Use it in the following\nformat:\n   sb find rack attack"]
          t.add_row ['help, h', 'Learn the various commands available through `sb <command>`']
          t.add_row ['hi', 'A warm greeting']
          t.add_row ['pods, p', 'Shows the amount of tickets for every view available in Zendesk']
          t.add_row ['self-hosted, sh, sm', 'Only shows the amount of tickets for self-managed']
          t.add_row ['services, s', 'Only shows the amount of tickets for gitlab.com']
          t.add_row ['slah status', 'Check if the bot can get the ticket overview']
          t.add_row ['slah test', 'Generate a test ticket message']
          t.add_row ['version', "Returns the version that SupportBot is running on #{SupportBot::VERSION}"]
          t.style = { all_separators: true, alignment: :left }
        end

        "```\n#{table}```"
      end
    end
  end
end
