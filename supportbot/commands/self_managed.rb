# frozen_string_literal: true

module SupportBot
  module Commands
    class SelfManaged < Base
      command 'self-managed', 'sm'
      views :need_org_triage, :sm_with_sla
    end
  end
end
