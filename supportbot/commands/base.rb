# frozen_string_literal: true

# Not a command, but class to inherit from in commands
module SupportBot
  module Commands
    class Base < SlackRubyBot::Commands::Base
      include ZendeskHelper
      include EmojiHelper

      def self.call(client, data, _match)
        @tickets = nil

        if tickets
          client.say(channel: data.channel, text: response)
        else
          client.say(channel: data.channel, text: 'Failed to retrieve tickets :cry:')
        end
      rescue StandardError => e
        client.say(channel: data.channel, text: "Sorry, #{e.message}.")
      end

      def self.tickets
        @tickets ||= ZendeskHelper.tickets_from_views
      end

      def self.views(*values)
        @views ||= values
      end

      # Overridable
      #
      def self.total
        @views.map { |v| tickets[v] }.sum
      end

      # Overridable
      #
      def self.response
        views = @views.map { |v| ZendeskHelper.views[v] }

        views_texts = views.map do |view|
          "#{view.name}: *#{tickets[view.type]}*"
        end

        views_texts.push("Total: #{EmojiHelper.get_ticket_emoji(total)}") if views_texts.count > 1

        views_texts.join("\n")
      end

      def self.logger
        @logger ||= zd_client.config.logger
      end
    end
  end
end
