# frozen_string_literal: true

module SupportBot
  module Commands
    class SelfHosted < Base
      command 'self-hosted', 'sh'

      def self.call(client, data, _match)
        client.say(channel: data.channel,
                   text: '`self-hosted` and `sh` have been deprecated, please use `self-managed` or `sm`')
      end
    end
  end
end
