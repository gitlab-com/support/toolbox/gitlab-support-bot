# frozen_string_literal: true

module SupportBot
  module Commands
    class Default < SlackRubyBot::Commands::Base
      command 'about'
      match(/^#{bot_matcher}$/u)

      def self.call(client, data, _match)
        client.say(channel: data.channel, text: SupportBot::Bot::ABOUT)
      end
    end
  end
end
