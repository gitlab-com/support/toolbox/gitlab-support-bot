module SupportBot
  module Hooks
    # Reaction Handler
    class ReactionRemoveHandler < SupportBot::Hooks::Base
      def call(client, data)
        return false if super

        case data.reaction
        when 'heavy_check_mark', 'white_check_mark'
          reaction_remover
        else
          logger.info "Unhandled Reaction #{data.reaction}"
        end
      end

      # When a user removes a :heavy_check_mark reaction
      def reaction_remover
        logger.info 'Reaction Removed'

        # Find ticket ID and Subject
        ticket = message.text.match(/\[(?<form>.*)\].*#(?<id>-?[0-9]*)>\s-\s(?<subject>.*)\s-\sresolved/)
        return false unless ticket

        build_message(
          "[#{ticket[:form]}] <#{SupportBot::ZENDESK_LOCATION}/agent/tickets/#{ticket[:id]}|##{ticket[:id]}> - #{ticket[:subject]}"
        )
      end

      def message
        super.blocks[0].elements[0]
      end
    end
  end
end
