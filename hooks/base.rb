# frozen_string_literal: true

module SupportBot
  module Hooks
    class Base
      attr_accessor :client, :data

      def call(client, data)
        self.client = client
        self.data = data

        message.bot_id == client.store.self.profile.bot_id
      end

      # Easy access to logger
      def logger
        @logger ||= zd_client.config.logger
      end

      # Helper to Find Slack Specific Message
      def message
        client.web_client.conversations_history(
          channel: data.item.channel, limit: 1, latest: data.item.ts, inclusive: true
        ).dig(:messages, 0)
      end

      def build_message(text = '', blocks = [])
        opts = {
          as_user: true, # Weird Otherwise
          channel: data.item.channel,
          blocks: blocks,
          text: text
        }

        client.web_client.chat_update(
          opts.merge(
            channel: data.item.channel,
            ts: data.item.ts
          )
        )
      end

      def username
        client.store.users[data.user].name
      end
    end
  end
end
