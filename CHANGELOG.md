### CHANGELOG ###

#### 0.3.4 ####
- Adds 'dc' command for "just .com" (Subscribers, Trials, Free)

#### 0.3.3 ####
- Update views for GitLab.com

#### 0.3.2 ####
- Add Changelog for the bot.
- Introduction to emoji helper.
