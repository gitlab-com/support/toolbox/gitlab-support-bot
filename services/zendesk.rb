# frozen_string_literal: true

require 'zendesk_api'
require 'dotenv/load'

def zd_client
  ZendeskApiClient.instance
end

class ZendeskApiClient < ZendeskAPI::Client
  def self.instance
    @instance ||= new do |config|
      config.url = "#{SupportBot::ZENDESK_LOCATION}/api/v2"
      config.username = 'support-ops@gitlab.com/token'
      config.password = ENV['ZD_TOKEN']

      config.retry = true
      config.cache = false

      if ENV['SB_ENVIRONMENT'] == 'dev'
        require 'logger'
        config.logger = Logger.new($stdout)
        config.logger.level = Logger::INFO
      end
    end
  end
end
