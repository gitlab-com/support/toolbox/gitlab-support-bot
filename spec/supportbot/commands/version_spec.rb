# frozen_string_literal: true

require 'spec_helper'

describe SupportBot do
  it 'has a version' do
    expect(SupportBot::VERSION).to_not be nil
  end
end
