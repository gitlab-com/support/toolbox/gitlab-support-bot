# frozen_string_literal: true

require 'spec_helper'

describe SupportBot::Commands::SelfHosted do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  context 'the commands' do
    it 'returns self-hosted tickets' do
      expect(message: "#{SlackRubyBot.config.user} self-hosted", channel: 'channel').to respond_with_slack_message(response)
    end

    it 'returns sh tickets' do
      expect(message: "#{SlackRubyBot.config.user} sh", channel: 'channel').to respond_with_slack_message(response)
    end
  end

  def response
    /`self-hosted` and `sh` have been deprecated, please use `self-managed` or `sm`/
  end
end
