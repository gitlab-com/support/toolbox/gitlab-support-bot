# frozen_string_literal: true

require 'spec_helper'

describe SupportBot::Commands::Default do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  context 'the commands' do
    it 'returns default response when called by name' do
      expect(message: SlackRubyBot.config.user, channel: 'channel').to respond_with_slack_message(SupportBot::Bot::ABOUT)
    end

    it 'returns default response for the about command' do
      expect(message: "#{SlackRubyBot.config.user} about", channel: 'channel').to respond_with_slack_message(SupportBot::Bot::ABOUT)
    end
  end
end
