# frozen_string_literal: true

require 'spec_helper'

describe SupportBot::Commands::Dotcom do
  include CommandsHelper

  subject(:app) { SupportBot::Bot.instance }

  context 'the commands' do
    it 'returns dotcom tickets' do
      expect(message: "#{SlackRubyBot.config.user} dotcom", channel: 'channel').to respond_with_slack_message(expected_response)
    end

    it 'returns dc tickets' do
      expect(message: "#{SlackRubyBot.config.user} dc", channel: 'channel').to respond_with_slack_message(expected_response)
    end
  end

  def expected_response
    need_org_triage = ticket_counts[:need_org_triage]
    dot_com = ticket_counts[:dot_com_with_sla]
    total = need_org_triage + dot_com

    /Needs Org & Triage: \*#{need_org_triage}\*\nGitLab.com with SLA: \*#{dot_com}\*\nTotal: \*#{total}\*/
  end
end
