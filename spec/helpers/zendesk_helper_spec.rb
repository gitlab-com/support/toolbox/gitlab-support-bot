# frozen_string_literal: true

require 'spec_helper'

describe ZendeskHelper do
  subject(:app) { ZendeskHelper }

  let!(:zd_client) { double }

  before do
    allow(app).to receive(:zd_client).and_return(zd_client)
  end

  context 'views' do
    it 'should return an array' do
      expect(ZendeskHelper.views).to be_a(Hash)
    end

    it 'should contain 4 views' do
      expect(ZendeskHelper.views.count).to eq(4)
    end

    it 'should have ZendeskView types' do
      expect(ZendeskHelper.views.first).to be_a(Object)
    end
  end

  context 'Search' do
    context '#search_for' do
      it 'runs a zd search' do
        expected_query = 'test_query type:ticket status:new status:open status:pending order_by:updated_at sort:desc'

        expect(zd_client).to receive(:search).with(per_page: 10, query: expected_query)

        app.search_for('test_query')
      end

      it 'runs a zd search with custom per_page setting' do
        expected_query = 'test_query type:ticket status:new status:open status:pending order_by:updated_at sort:desc'

        expect(zd_client).to receive(:search).with(per_page: 200, query: expected_query)

        app.search_for('test_query', 200)
      end
    end

    context '#ticket_support_level' do
      it 'returns N/A when the org does not exist' do
        result = app.ticket_support_level(mock_ticket)

        expect(result).to eq('N/A')
      end

      it 'returns the Support Level when it exists' do
        basic = app.ticket_support_level(mock_ticket('basic'))
        premium = app.ticket_support_level(mock_ticket('premium'))
        silver = app.ticket_support_level(mock_ticket('silver'))
        gold = app.ticket_support_level(mock_ticket('gold'))

        expect(basic).to eq('Basic')
        expect(premium).to eq('Premium')
        expect(silver).to eq('Silver')
        expect(gold).to eq('Gold')
      end
    end
  end

  context 'SLAH Query' do
    let(:views) { double }
    let(:zd_tickets) { double }
    let(:zd_config) { double }

    before do
      allow(zd_config).to receive(:logger).and_return(Logger.new(nil))
      allow(zd_tickets).to receive(:tickets).with(anything).and_return(mock_tickets)

      allow(views).to receive(:find!).and_return(zd_tickets)
      allow(zd_client).to receive(:views).and_return(views)
      allow(zd_client).to receive(:config).and_return(zd_config)
    end

    it 'runs a search' do
      tickets = ZendeskHelper.slah_query
      expect(tickets.count).to eq(12)
    end
  end

  context 'Slah tickets' do
    let(:views) { double }
    let(:zd_tickets) { double }

    before do
      allow(zd_tickets).to receive(:tickets).with(anything).and_return([])
      allow(views).to receive(:find!).and_return(zd_tickets)
      allow(zd_client).to receive(:views).and_return(views)
    end

    it 'Should return an array' do
      expect(
        ZendeskHelper.slah_tickets
      ).to eq([])
    end
  end

  context 'Previously reported' do
    it 'Should return same ticket list' do
      expect(
        ZendeskHelper.only_unreported(mock_tickets).count
      ).to eq(12)
    end

    it 'should only return not cached tickets' do
      tickets = mock_tickets(100)
      last_tickets = tickets.pop

      expect(
        ZendeskHelper.only_unreported(tickets).count
      ).to eq(11)

      tickets.push(last_tickets)
      expect(
        ZendeskHelper.only_unreported(tickets)
      ).to eq([last_tickets])
    end
  end

  context 'Find active SLA' do
    it 'Should return empty array' do
      ticket = mock_tickets.first
      ticket.slas['policy_metrics'] = {}

      expect(ZendeskHelper.find_active_slah(ticket)).to eq(nil)
    end

    it 'Should return active SLA' do
      ticket = mock_tickets.first
      ticket.slas['policy_metrics'] = [{
        stage: 'active'
      }]

      expect(ZendeskHelper.find_active_slah(ticket)).to eq({ 'stage' => 'active' })
    end
  end

  private

  def mock_tickets(id = 1)
    statuses = %w[new open pending hold solved closed]
    tags = %w[tag1 tag2]
    slas = [
      { 'policy_metrics': [{ stage: 'active', breach_at: Time.now + 1.hours }] },
      { 'policy_metrics': [{ stage: 'active', breach_at: Time.now + 4.hours }] }
    ]

    statuses.map do |status|
      tags.map do |tag|
        id += 1
        ZendeskAPI::Ticket.new(app, id: id, status: status, tags: [tag], slas: (id.even? ? slas[0] : slas[1]))
      end
    end.flatten
  end

  def mock_ticket(support_level = nil)
    mock_ticket = ZendeskAPI::Ticket.new(app) # doesn't actually create it until `.save` is called
    return mock_ticket if support_level.nil?

    mock_ticket.organization = ZendeskAPI::Organization.new(app)
    mock_ticket.organization.organization_fields = { support_level: support_level.to_s }
    mock_ticket
  end
end
