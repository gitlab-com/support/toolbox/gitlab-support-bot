# frozen_string_literal: true

require 'spec_helper'

describe 'ReactionRemoveHandler' do
  subject(:app) { SupportBot::Hooks::ReactionRemoveHandler.new }

  let!(:client) { instance_double(SlackRubyBot::Client) }
  let!(:data) do
    double(
      reaction: 'heavy_check_mark',
      type: 'reaction_removed',
      user: :test,
      item: double(channel: 'channel', ts: 'ts')
    )
  end
  let!(:test_user) { double }

  before do
    allow(test_user).to receive(:name).and_return('test bot')
    allow(app).to receive(:message).and_return(double(bot_id: 123_456))
    allow(client).to receive(:store).and_return(
      double(
        users: { test: test_user },
        self: double(
          profile: double(bot_id: 654_321)
        )
      )
    )
  end

  context 'call' do
    let!(:info) { double(info: nil) }

    it 'should call reaction_remove' do
      allow(app).to receive(:reaction_remover).and_return(true)

      expect(app.call(client, data)).to eq(true)
    end

    it 'should return log message' do
      data = double(reaction: 'something')
      allow(app).to receive(:logger).and_return(info)

      app.call(client, data)

      expect(info).to have_received(:info)
    end
  end

  context 'reaction_resolve' do
    it 'should return false on same bot id check' do
      allow(app).to receive(:message).and_return(double(bot_id: 654_321))

      expect(app.call(client, data)).to eq(false)
    end

    it 'should return false on ticket section' do
      allow(app).to receive(:message).and_return(double(bot_id: 123_456, text: 'Welcome'))

      expect(app.call(client, data)).to eq(false)
    end

    it 'should return' do
      allow(app).to receive(:message).and_return(double(bot_id: 123_456, text: '[sm] <agent/tickets/-123|#-123> - Test ticket - resolved'))
      allow(app).to receive(:alert_resolve)
      allow(client).to receive(:web_client).and_return(double(chat_update: nil))

      app.call(client, data)

      expect(client).to have_received(:web_client)
    end
  end
end
